/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
]

let friendsList = [];

let new_User;
let new_Friend;

// item 1.

function add_Another_User(){
    if(confirm("Add another user?")){
        registration();
    }
}
function username_checker(){
    let username_found = registeredUsers.includes(new_User);
    if(new_User === null){
        alert("Nothing entered")
        add_Another_User();
    }else if (username_found === true){
        alert("Registration failed. Username already exists!")
            add_Another_User()
    }else if(new_User === "" ){
        alert("Nothing entered")
        add_Another_User();
    } else {
        alert("Thank you for registering!")

        let add_User = registeredUsers.push(new_User);
        add_Another_User();
    }
}
function registration(){
    new_User = prompt("Enter username: ")
    username_checker();    
}

registration();
console.log(registeredUsers);


// item 2.

function add_Another_Friend(){
    if(confirm("Add another friend?")){
        add_Friend();
    }
}

function addFriend_checker(){
    let username_found = registeredUsers.includes(new_Friend);

    if(new_Friend === null){
        alert("Nothing entered")
        add_Another_Friend();
    }else if (username_found === false){
        alert("User not found.")
            add_Another_Friend()
    }else if(new_Friend === ""){
        alert("Nothing entered")
        add_Another_Friend();
    } else {
        alert("You have added " + new_Friend + " as a friend!")

        let list_Friend = friendsList.push(new_Friend);
        add_Another_Friend();
    }
}
function add_Friend(){
    new_Friend = prompt("Enter username to add as a friend: ")
    addFriend_checker();    
}

add_Friend();
console.log(friendsList);

// item 3.
function view_Friends(){
    if (friendsList == 0 ){
        console.log("You currently have 0 friends. Add one first.");
    }
    friendsList.forEach(function(friends){
        console.log(friends);
        
    
    })
}

view_Friends();

// item 4

function numberOf_Friends(){
    if (friendsList.length > 0){
        console.log("You currently have " +friendsList.length +" friend/s.");
    }else{ 
        console.log("You currently have 0 friends. Add one first.");
    }
}

numberOf_Friends();

// item 5.

function delete_LastFriend(){
    if (friendsList.length > 0){
        friendsList.pop();
        console.log("last friend in the list removed");
    } else {
        console.log("You currently have 0 friends. Add one first.")
    };
};

delete_LastFriend();
console.log(friendsList);

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/





